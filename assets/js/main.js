(function($) {

	$(document).ready(function($) {

		// Get real window width
		function getWindowWidth() {
			var windowWidth = 0;
			if ( jQuery.type( $(window).innerWidth() ) === 'number' ) {
				windowWidth = $(window).innerWidth();
			} else {
				if ( $(document) && $(document).width()) {
					windowWidth = $(document).width();
				} else {
					if ( $(document.body) && $(document.body).width() ) {
						windowWidth = $(document.body).width();
					}
				}
			}
			return windowWidth;
		}

		// Responsive issues.
		function isMobile() {
			var ww = getWindowWidth();
			if ( ww < 480 ) {
				return 'mobile';
			} else if ( ww >= 480 && ww < 992 ) {
				return 'breakpoint';
			} else if ( ww >= 992 ) {
				return false;
			}
		}

		// Get scrolling class.
		function getScrollingClass() {
			$( document ).scroll( function() {
				if ( is_Mobile === false ) {
					var top = $( document ).scrollTop(),
						$header = $( 'header .navbar' ),
						header_height = $header.height();

					if ( top > header_height ) {
						$( 'body' ).addClass( 'scrolling' );
					} else {
						$( 'body' ).removeClass('scrolling' );
					}
				}
			});
		}

		// On initial page load.
		var is_Mobile = isMobile();
		if ( is_Mobile !== false ) {
			$( 'body' ).addClass( 'scrolling' );
		} else {
			getScrollingClass();
		};

		// Hold document body vertical scroll when mobile navigation is collapsed.  
		if( is_Mobile !== false ) {
			$( '.navbar-toggle' ).click( function() {
				if( $(this).hasClass( 'collapsed' ) ) {
					$( 'body, html' ).css( 'overflow', 'hidden' );
				} else {
					$( 'body, html' ).css( 'overflow', 'auto' );
				}
			});
		}

		// On windows resize.
		$(window).bind('resize orientationchange', function() {
			is_Mobile = isMobile();
			if ( is_Mobile === 'mobile' || is_Mobile === 'breakpoint' ) {
				$( 'body' ).addClass( 'scrolling' );
			} else {
				$( 'body' ).removeClass( 'scrolling' );
				getScrollingClass();
				setTimeout(getMatchHeight, 500);
			}
		});

	});

})(jQuery);
